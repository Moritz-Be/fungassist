<?php


namespace App\Tests\Service;


use App\Service\AnchorService;
use PHPUnit\Framework\TestCase;

class AnchorServiceTest extends TestCase
{
    /**
     * @dataProvider provideTestRemoveAnchorFromStringData
     */
    public function testRemoveAnchorFromString(string $string, string $expectedResult){

        $anchorService = new AnchorService();

        $result = $anchorService->removeAnchorFromString(15, $string);

        $this->assertEquals($expectedResult, $result);
    }

    public function provideTestRemoveAnchorFromStringData(){
        return [
            'empty String' => ['', ''],
            'only numbers String' => ['1515', '1515'],
            'string and numbers' => ['15 is great', '15 is great'],
            'start example' => ['=====start_anchor_15_start=====', ''],
            'end example' => ['=====end_anchor_15_end=====', ''],
        ];
    }

}