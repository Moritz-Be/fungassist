<?php


namespace App\Tests\Service;


use App\Entity\Algorithm;
use App\Repository\AlgorithmRepository;
use App\Repository\QuestionItemRepository;
use App\Service\AlgorithmService;
use PHPUnit\Framework\TestCase;

class AlgorithmServiceTest extends TestCase
{

    private $ifStatement = 'ifStatement';

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $algorithmRepository;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $questionItemRepository;

    private $algorithmService;

    private $algorithm;

    public function setUp(){


        $this->algorithmRepository = $this->createMock(AlgorithmRepository::class);

        $this->questionItemRepository = $this->createMock(QuestionItemRepository::class);

        $this->algorithmService = new AlgorithmService($this->algorithmRepository, $this->questionItemRepository);

    }

    /**
     * @param string $inputString
     * @param string $expectedResult
     * @dataProvider provideParseAlgorithmData
     */
    public function testParseAlgorithm(string $inputString, string $expectedResult){

        $this->algorithm = new Algorithm();
        $this->algorithm->setIfStatement($this->ifStatement);

        $this->algorithmRepository->method('findOneBy')->willReturn($this->algorithm);

        $result = $this->algorithmService->parseAlgorithm($inputString);

        $this->assertEquals($expectedResult, $result);
    }

    public function provideParseAlgorithmData(){
        return [
            'nonsense' => ['asdfasdfasdf', 'asdfasdfasdf'],
            'relevantStrings' => ['{"algorithm":10}', $this->ifStatement],
            'relevantStrings1' => ['{   "algorithm":10}', $this->ifStatement],
            'relevantStrings2' => ['{   "   algorithm ":  10}', $this->ifStatement],
            'relevantStrings3' => ["{\"algorithm\"   \n :   10}", $this->ifStatement],
            'relevantStrings4' => ['    { " algorithm":10    }    ', '    '.$this->ifStatement.'    '],
        ];
    }
}