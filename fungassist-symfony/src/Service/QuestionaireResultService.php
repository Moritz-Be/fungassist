<?php


namespace App\Service;


use App\Entity\QuestionaireResult;
use App\Repository\AnchorRepository;
use App\Repository\QuestionaireRepository;
use App\Repository\QuestionaireResultRepository;
use App\Repository\QuestionItemRepository;
use Doctrine\ORM\EntityManagerInterface;

class QuestionaireResultService
{
    private $anchorRepository;

    private $questionItemRepository;

    private $questionaireRepository;

    private $entityManager;

    private $questionaireResultRepository;

    public function __construct(AnchorRepository $anchorRepository, QuestionItemRepository $questionItemRepository, QuestionaireRepository $questionaireRepository, QuestionaireResultRepository $questionaireResultRepository, EntityManagerInterface $entityManager)
    {
        $this->anchorRepository = $anchorRepository;
        $this->questionaireResultRepository = $questionaireResultRepository;
        $this->questionItemRepository = $questionItemRepository;
        $this->questionaireRepository = $questionaireRepository;

        $this->entityManager = $entityManager;
    }


    public function newQuestionaireResult($questionaireTitle){
        $questionaireResult = new QuestionaireResult();
        $questionaireResult->setSlug(rand(1000000, 10000000000));
        $questionaireResult->setCreatedAt(new \DateTime('now'));
        $questionaireResult->setQuestionaire($this->questionaireRepository->findOneBy(['title'=>$questionaireTitle]));

        $this->entityManager->persist($questionaireResult);
        $this->entityManager->flush();

        return $questionaireResult;
    }

    public function updateQuestionaireResult($POSTData){

        $questionaireResult =$this->questionaireResultRepository->findOneBy(['slug'=>$POSTData['slug']]);

        // stop modifying final questionaire results
        if($questionaireResult->getCmt() == 'finalSubmit'){
            $questionaireResult = $this->newQuestionaireResult($questionaireResult->getQuestionaire()->getTitle());
        }

        // flag as final if it's submit
        if(isset($POSTData['flag'])){
            $questionaireResult->setCmt($POSTData['flag']);

        }

        // remove previous anchors
        foreach($questionaireResult->getAnchorsDisplayed() as $anchor){
            $questionaireResult->removeAnchorsDisplayed($anchor);
        }

        // get and set all anchors
        if(isset($_POST['anchorIds'])){
            $anchors = $this->anchorRepository->findBy(['id'=>$POSTData['anchorIds']]);
            foreach($anchors as $anchor){
                $questionaireResult->addAnchorsDisplayed($anchor);
            }
        }

        // remove past questionItems
        foreach($questionaireResult->getSelectedQuestionItems() as $questionItem){
            $questionaireResult->removeSelectedQuestionItem($questionItem);
        }

        // get and set all questionItems
        $questionItems=[];
        foreach($POSTData['questionItems'] as $parent=>$qpuestionItemIds){
            if(is_array($qpuestionItemIds)){
                $questionItems=array_merge($questionItems, $qpuestionItemIds);
            } else {
                $questionItems[]=$qpuestionItemIds;
            }

        }

        $questionItems = $this->questionItemRepository->findBy(['id'=>$questionItems]);
        foreach($questionItems as $questionItem){
            $questionaireResult->addSelectedQuestionItem($questionItem);
        }

        $this->entityManager->persist($questionaireResult);
        $this->entityManager->flush();
        return $questionaireResult;
    }

}