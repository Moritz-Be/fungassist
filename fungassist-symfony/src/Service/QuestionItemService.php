<?php


namespace App\Service;

use App\Entity\Algorithm;
use App\Entity\Questionaire;
use App\Entity\QuestionaireResult;
use App\Entity\QuestionItem;
use App\Service\AlgorithmService;
use App\Repository\AlgorithmRepository;
use App\Repository\QuestionItemRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class QuestionItemService
{
    private $questionItemRepository;

    private $entityManager;

    private $algorithmService;

    private $algorithmRepository;

    public function __construct(AlgorithmRepository $algorithmRepository, QuestionItemRepository $questionItemRepository, EntityManagerInterface $entityManager, AlgorithmService $algorithmService)
    {
        $this->questionItemRepository = $questionItemRepository;
        $this->algorithmRepository = $algorithmRepository;
        $this->entityManager = $entityManager;
        $this->algorithmService = $algorithmService;
    }

    /**
     * @param QuestionItem $questionItem
     * @return QuestionItem
     */
    public function getChildrenItems(QuestionItem $questionItem)
    {
        if(empty($questionItem->getChildren())){

            // find a item where parentID = $questionItem->getId() AND WHERE itemAbove = $questionItem->getId()
            $previousQuestionItemId = $questionItem->getId();
            $i = 1;
            while ($i != null) {

                // find the next item
                $nextQuestionItem = $this->questionItemRepository->findOneBy(['parentId' => $questionItem->getId(), 'itemAbove' => $previousQuestionItemId]);
                // add it to the list of children
                if ($nextQuestionItem != null) {
                    $questionItem->addChild($nextQuestionItem);
                    // set the newly found QuestionItem for the next query
                    $previousQuestionItemId = $nextQuestionItem->getId();
                } else {
                    $i = null;
                }

            }

            return $questionItem;
        } else{
            return $questionItem;
        }


    }

    /**
     * generate Questionitem Tree within the parent questionItem as children
     * @param Questionaire $questionaire
     * @return QuestionItem|null
     */
    public function generateQuestionItemTree(Questionaire $questionaire)
    {

        $questionItemParent = $this->questionItemRepository->findOneBy(['questionaire' => $questionaire, 'type' => QuestionItem::TYPE['PARENT']]);

        // get all selectables

        $questionItemParent = $this->getChildrenItems($questionItemParent);
        if ($questionItemParent->getChildren() != null) {
            // get all options
            foreach ($questionItemParent->getChildren() as $questionItem) {
                if ($questionItem != null) {
                    $questionItem = $this->getChildrenItems($questionItem);
                }
            }
        }

        return $questionItemParent;
    }

    /**
     * @param QuestionItem $questionItem
     * @return bool
     */
    public function moveQuestionItemUp(QuestionItem $questionItem)
    {

        // QuestionItem is already on top, then do nothing
        if ($questionItem->getParentId() == $questionItem->getItemAbove()) {
            return true;
        }

        // get relevant QuestionItems
        $questionItemAbove = $this->questionItemRepository->findOneBy(['id' => $questionItem->getItemAbove()]);
        $questionItemUnderneath = $this->questionItemRepository->findOneBy(['itemAbove' => $questionItem->getId()]);

        // there might be 2 items with same ItemAbove
        // one SINGLE/MULTI_SELECT and one of it's option
        // therefore get only the SINGLE/MULTI_SELECT
        $questionItemUnderneaths = $this->questionItemRepository->findBy(['itemAbove' => $questionItem->getId()]);
        if (count($questionItemUnderneaths) > 1) {
            foreach ($questionItemUnderneaths as $possibleQuestionItemUnderneath) {
                if ($possibleQuestionItemUnderneath->getParentId() == $questionItem->getParentId()) {
                    $questionItemUnderneath = $possibleQuestionItemUnderneath;
                }
            }
        }

        if ($questionItemUnderneath == null) {
            $questionItem->setItemAbove($questionItemAbove->getItemAbove());
            $questionItemAbove->setItemAbove($questionItem->getId());

            $this->entityManager->persist($questionItem);
            $this->entityManager->persist($questionItemAbove);
        } else {
            $questionItem->setItemAbove($questionItemAbove->getItemAbove());
            $questionItemAbove->setItemAbove($questionItem->getId());
            // For SINGLE-SELECT and MULTI-SELECT, when they are moved upward
            // their first option's item above should always stay the same!
            if ($questionItemUnderneath->getParentId() != $questionItem->getId()) {
                $questionItemUnderneath->setItemAbove($questionItemAbove->getId());
            }
            $this->entityManager->persist($questionItem);
            $this->entityManager->persist($questionItemAbove);
            $this->entityManager->persist($questionItemUnderneath);
        }

        $this->entityManager->flush();

        return true;

    }

    /**
     * @param QuestionItem $questionItem
     * @return bool
     */
    public function moveQuestionItemDown(QuestionItem $questionItem)
    {
        // get relevant QuestionItems
        $questionItemUnderneath = $this->questionItemRepository->findOneBy(['itemAbove' => $questionItem->getId()]);

        // OPTION item is the last item
        if ($questionItemUnderneath == null) {
            return true;
        }

        // to get SINGLE/MULTI_SELECT
        $questionItemUnderneaths = $this->questionItemRepository->findBy(['itemAbove' => $questionItem->getId()]);
        if (count($questionItemUnderneaths) > 1) {
            foreach ($questionItemUnderneaths as $possibleQuestionItemUnderneath) {
                if ($possibleQuestionItemUnderneath->getParentId() == $questionItem->getParentId()) {
                    $questionItemUnderneath = $possibleQuestionItemUnderneath;
                }
            }
        }

        // SINGLE/MULTI_SELECT as last item. (when press down on last SINGLE/MULTI_SELECT)
        if ($questionItemUnderneath->getParentId() == $questionItemUnderneath->getItemAbove()) {
            return true;
        }

        $questionItemUnderneathTwo = $this->questionItemRepository->findOneBy(['itemAbove' => $questionItemUnderneath->getId()]);

        if ($questionItemUnderneathTwo != null) {
            // to get SINGLE/MULTI_SELECT
            $questionItemUnderneathsTwo = $this->questionItemRepository->findBy(['itemAbove' => $questionItemUnderneath->getId()]);
            if (count($questionItemUnderneathsTwo) > 1) {
                foreach ($questionItemUnderneathsTwo as $possibleQuestionItemUnderneath) {
                    if ($possibleQuestionItemUnderneath->getParentId() == $questionItemUnderneath->getParentId()) {
                        $questionItemUnderneathTwo = $possibleQuestionItemUnderneath;
                    }
                }
            } else if ($questionItemUnderneathTwo->getParentId() == $questionItemUnderneath->getId()) {
                $questionItemUnderneathTwo = null;
            }
        }

        // just one item below the item
        if ($questionItemUnderneathTwo == null) {
            $questionItemUnderneath->setItemAbove($questionItem->getItemAbove());
            $questionItem->setItemAbove($questionItemUnderneath->getId());
            $this->entityManager->persist($questionItem);
            $this->entityManager->persist($questionItemUnderneath);
        } else {
            // two or more items below the item
            $questionItemUnderneath->setItemAbove($questionItem->getItemAbove());
            $questionItem->setItemAbove($questionItemUnderneath->getId());
            $questionItemUnderneathTwo->setItemAbove($questionItem->getId());
            $this->entityManager->persist($questionItem);
            $this->entityManager->persist($questionItemUnderneath);
            $this->entityManager->persist($questionItemUnderneathTwo);
            // echo("hi");
        }

        $this->entityManager->flush();

        return true;

    }

    /**
     * @param QuestionItem $questionItem
     * @return bool
     */
    public function deleteQuestionItem(QuestionItem $questionItem)
    {
        $questionItemAbove = $this->questionItemRepository->findOneBy(['id' => $questionItem->getItemAbove()]);
        $questionItemUnderneath = $this->questionItemRepository->findOneBy(['itemAbove' => $questionItem->getId()]);

        if ($questionItem->getType() != "OPTION") {
            $questionItemUnderneaths = $this->questionItemRepository->findBy(['itemAbove' => $questionItem->getId()]);
            if (count($questionItemUnderneaths) > 1) {
                foreach ($questionItemUnderneaths as $possibleQuestionItemUnderneath) {
                    if ($possibleQuestionItemUnderneath->getParentId() == $questionItem->getParentId()) {
                        $questionItemUnderneath = $possibleQuestionItemUnderneath;
                    }
                }
            }
        } 
        // If it is parent of options then remove options as well!
        if ($questionItem->getType() == "SINGLE_SELECT" || $questionItem->getType() == "MULTI_SELECT") {
            $questionItemOptions = $this->questionItemRepository->findBy(['parentId' => $questionItem->getId()]);
            foreach($questionItemOptions as $questionItemOption){
                $this->entityManager->remove($questionItemOption);
            }
            if ($questionItemUnderneath->getParentId() == $questionItem->getId()) {
                $questionItemUnderneath = null;
            }
        }

        if ($questionItemUnderneath == null) {
            // $questionItem->setItemAbove(0);
            $this->entityManager->remove($questionItem);
            // $this->entityManager->persist($questionItem);
        } else {
            $questionItemUnderneath->setItemAbove($questionItemAbove->getId());
            // $questionItem->setItemAbove(0);
            $this->entityManager->remove($questionItem);
            $this->entityManager->persist($questionItemUnderneath);
            // $this->entityManager->persist($questionItem);
        }

        $this->entityManager->flush();
        // $this->entityManager->remove($questionItem);
        // $this->entityManager->flush();

        return true;

    }

    /**
     * @param QuestionItem $questionItem
     * @return bool
     */
    public function deleteDisplayCondition(QuestionItem $questionItem)
    {   
        $questionItem = $this->questionItemRepository->findOneBy(['id' => $questionItem->getId()]);
        $questionItem->setDisplayCondition('');
        $this->entityManager->persist($questionItem);
        $this->entityManager->flush();
        return true;
    }



    /**
     * find the correct itemAbove Id for the given $questionItem and return the object with it's correct itemAbove Property set.
     * @param QuestionItem $questionItem
     * @return QuestionItem
     */
    public function setItemAboveId(QuestionItem $questionItem, QuestionItem $parent)
    {

        $parent = $this->getChildrenItems($parent);

        // set itemAbove to parent if parent has no children, else use the last child
        if (empty($parent->getChildren())) {
            $questionItem->setItemAbove($parent->getId());
        } else {
            /**
             * @var $lastChild QuestionItem
             */
            $children = $parent->getChildren();
            $lastChild = end($children);
            $questionItem->setItemAbove($lastChild->getId());
        }

        return $questionItem;
    }

    /**
     * generate a text case Report
     * @param QuestionaireResult $questionaireResult
     * @return bool|mixed|string|null
     */
    public function generatePatientReport(QuestionaireResult $questionaireResult){

        if($questionaireResult->getQuestionaire()->getCaseReportTemmplate() === null){
            return false;
        } else{
            $patientReport = $questionaireResult->getQuestionaire()->getCaseReportTemmplate();

            $selectedQuestionItems = [];
            foreach($questionaireResult->getSelectedQuestionItems() as $questionItem){
                $selectedQuestionItems[$questionItem->getId()]=$questionItem;
            }


            // search for all %%questionId%% to replace stuff
            preg_match_all('/(?<=%%)\d+(?=%%)/', $patientReport, $matches);


            // loop over the found spots
            foreach($matches[0] as $match){
                $hits=[];

                // get all childs of the parent questionItem
                $parent = $this->getChildrenItems($this->questionItemRepository->findOneBy(['id'=>$match]));

                // check which of these is selected in the questionaireResult object
                foreach($parent->getChildren() as $child){
                    if(array_key_exists($child->getId(), $selectedQuestionItems)){
                        $hits[]=$child->getContent();
                    }
                }

                // implode the content of these
                $string = implode(', ', $hits);

                // replace the %%questionId%% with this string
                $patientReport = str_replace('%%'.$match.'%%', $string, $patientReport);


            }
            return $patientReport;
        }

    }

    /**
     * @param array $selectedQuestionItems
     * @param Questionaire $questionaire
     * @return array
     */
    public function assessQuestionItemsToDisplay($selectedQuestionItemIds, Questionaire $questionaire){
        // get all questionItems of the questionaire
        $questionItems = $this->questionItemRepository->findBy(['questionaire'=>$questionaire]);

        // loop over them and execute the displayCondition with the given $selectedQuestionItems
        $assessedQuestionItems = [];
        foreach($questionItems as $questionItem){
            if($questionItem->getDisplayCondition() == null){
                // if no condition was set display this questionItem
                $assessedQuestionItems[]=['id'=>$questionItem->getId(), 'state'=>true];
            } else {
                // otherwise execute the algorithm
                $assessedQuestionItems[]=$assessedQuestionItems[]=['id'=>$questionItem->getId(), 'state'=>$this->algorithmService->executeAlgorithm($questionItem->getDisplayCondition(), $selectedQuestionItemIds)];
            }
        }

        // return an array with 'questionItemId'=>bool
        return $assessedQuestionItems;

        }

    
    /**
     * @param $algorithmString
     * @return string
     */
    public function parseAlgorithm($algorithmString){

        // if($this->parseAlgorithmCount > AlgorithmService::MAX_ALGORITHM_INHERITANCE){
        //     throw new \ErrorException('Your algorithm includes too many references');
        // }


        //       {\s*"\s*algorithm\s*"\s*:\s*(\d+)\s*}
        preg_match_all('/{\s*"\s*algorithm\s*"\s*:\s*(\d+)\s*}/', $algorithmString, $matches);
        foreach ($matches[1] as $match){
            // find the relevant algorithm
            $algorithm = $this->algorithmRepository->findOneBy(['id'=>$match]);
            $algorithmString = preg_replace('/{\s*"\s*algorithm\s*"\s*:\s*'.$match.'\s*}/', $algorithm->getIfStatement(), $algorithmString);
        }

        // Increase the parse Algorithm Count to ensure no loops
        // $this->parseAlgorithmCount++;

        // check for new referenced algorithms
        preg_match_all('/{\s*"\s*algorithm\s*"\s*:\s*(\d+)\s*}/', $algorithmString, $matches);
        if(!empty($matches[0])){
            $this->parseAlgorithm($algorithmString);
        } else {
            return $algorithmString;
        }
    }

    /**
     * @param string $questionItemDisplayCondition
     * @param QuestionItem $questionItem
     * @return bool
     */
    private function checkForFalseQuestionItem($questionItemDisplayCondition, QuestionItem $questionItem){
        preg_match('/":\s*'.$questionItem->getId().'\s*}\s*,\s*false/', $questionItemDisplayCondition, $m);
        // preg_match('/"'.$questionItem->getId().'"\s*}\s*,\s*f/', $algorithm->getIfStatement(), $m);

        if(empty($m)){
            return false;
        } else{
            return true;
        }
    }

    /**
     * add cmts to the readable form of a questionItem
     * @param string $questionItemDisplayCondition
     * @param QuestionItem $questionItem
     * @return string
     */
    private function addCmtToReadableQuestionItem($questionItemDisplayCondition, QuestionItem $questionItem){
        $cmt = [];

        if($this->checkForFalseQuestionItem($questionItemDisplayCondition, $questionItem)){
            $cmt []= "<span style='color:red;'>(IF NOT TRUE!)</span>";
        }

        return implode(' ', $cmt);
    }


    /**
     * function to generate display condition
     * @param int $selectedQuestionItemId
     * @param QuestionItem $questionItem
     * @return QuestionItem
     */
    public function generateDisplayCondition($selectedQuestionItemId)
    {

        // prepare display condition for inherited questionItems 
        $questionItem = $this->questionItemRepository->findOneBy(['id'=>$selectedQuestionItemId]);
        $questionItemDisplayCondition = $questionItem->getDisplayCondition();
        $questionItemDisplayCondition = $this->parseAlgorithm($questionItemDisplayCondition);

        if ($questionItemDisplayCondition == null) {
            return null;
        }

        $json = json_decode($questionItemDisplayCondition, true);


        $implodedDisplayCondition = $this->multi_implode($json, ' ');

        preg_match_all('!\d+!', $implodedDisplayCondition, $matches);

        $questionItems = $this->questionItemRepository->findBy(['id' => array_map('intval', $matches[0])]);

        foreach ($questionItems as $questionItem) {
            $parentQuestionitem = $this->questionItemRepository->findOneBy(['id' => $questionItem->getParentId()]);

            $cmt = $this->addCmtToReadableQuestionItem($questionItemDisplayCondition, $questionItem);

            $implodedDisplayCondition =  str_replace('--' . $questionItem->getId() . '--', strtolower($parentQuestionitem->getContent() . ': <strong>'. $questionItem->getContent(). ' '. $cmt .'</strong>'), $implodedDisplayCondition);
        }
        return $implodedDisplayCondition;
    }

    /**
     * @param $array array
     * @param $glue string
     * @return bool|string
     */
    private function multi_implode($array, $glue)
    {
        $ret = '';

        foreach ($array as $key => $item) {
            if (is_array($item)) {

                $ret .= ' ' . $this->multi_implode($item, '<br><br>' . strtoupper($key) . '<br><br>') . $glue;

            } else {
                if ($key === 'var') {
                    $ret .= '--' . $item . '-- ' . $glue;
                }

            }
        }

        $ret = substr($ret, 0, 0 - strlen($glue));

        return $ret;
    }
}