<?php


namespace App\Service;


class GuidelineSourceService
{

    /*
    helpful regexes:

    remove space between the first comma and a bracket: (?<=,)(?:.(?!"))*(?=\()

    */


    /**
     * @param $string
     * @return mixed
     */
    public function queryForPMIDOnPubMed($string)
    {
        $contents = file_get_contents('https://www.ncbi.nlm.nih.gov/search/all/?term=' . urlencode($string));

        preg_match('/(?<=pubmed\/)\d+/', $contents, $matches);

        if (empty($matches)) {
            $contents = file_get_contents('https://www.ncbi.nlm.nih.gov/pubmed/?term=' . urlencode($string));
            $contents = preg_replace('/\s+/', '', $contents);
            preg_match('/(?<=PMID:<\/dt><dd>)\d+/', $contents, $matches);
        }

        return $matches;

    }

    /**
     * @param array $pmids
     * @return array
     */
    public function getPubMedSitesByPMIDs(array $pmids)
    {

        // create empty array
        $pubmedSources = array();

        foreach ($pmids as $pmid) {

            $pubmedSources[$pmid] = $this->getPubMedSiteByPMID($pmid);

        }
        return $pubmedSources;
    }

    /**
     * @param $pmid
     * @return string
     */
    public function getPubMedSiteByPMID($pmid)
    {
        $contents = file_get_contents('https://www.ncbi.nlm.nih.gov/pubmed/' . $pmid);

        $contents = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $contents);
        $contents = strstr($contents, '<html');

        return addslashes(str_replace(PHP_EOL, '', $contents));
    }


}