<?php


namespace App\Service;


use App\Entity\Anchor;

class AnchorService
{

    /**
     * @param int $anchorId
     * @return string
     */
    public function createAnchorStartString(int $anchorId)
    {
        return str_replace('id', $anchorId, Anchor::ANCHOR_LAYOUT['start']);
    }

    /**
     * @param int $anchorId
     * @return string
     */
    public function createAnchorEndString(int $anchorId)
    {
        return str_replace('id', $anchorId, Anchor::ANCHOR_LAYOUT['end']);
    }


    /**
     * Create the string for the anchor
     * @param string $text
     * @param int $anchorId
     * @return string
     */
    public function createAnchorString(string $text, int $anchorId)
    {

        return $this->createAnchorStartString($anchorId) . $text . $this->createAnchorEndString($anchorId);
    }

    /**
     * @param Anchor $anchor
     * @return Anchor
     */
    public function removeSameAnchorFromAnchorText(Anchor $anchor)
    {
        return $anchor->setText($this->removeAnchorFromString($anchor->getId(), $anchor->getText()));
    }

    /**
     * given any string remove the anchor with a given ID from it
     * @param int $anchorId
     * @param string $string
     * @return string|string[]|null
     */
    public function removeAnchorFromString(int $anchorId, string $string)
    {
        return preg_replace([
            '/' . $this->createAnchorStartString($anchorId) . '/',
            '/' . $this->createAnchorEndString($anchorId) . '/',
        ], [
            '',
            '',
        ], $string);
    }


}