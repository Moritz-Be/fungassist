<?php


namespace App\Service;


use FOS\UserBundle\Mailer\MailerInterface;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FOSUserSendgridMailer implements MailerInterface
{
    private $mailer;

    private $router;

    public function __construct(\Symfony\Component\Mailer\MailerInterface $mailer, UrlGeneratorInterface $router)
    {
        $this->mailer = $mailer;
        $this->router = $router;
    }

    public function sendConfirmationEmailMessage(UserInterface $user)
    {
        $email = (new Email())
            ->from('info@fungassist.net')
            ->to($user->getEmail())
            ->subject('Registration FungAssist')
            ->html('<p>Please follow this link to activate your account:</p>'. $this->router->generate('fos_user_registration_confirm', ['token'=>$user->getConfirmationToken()], UrlGeneratorInterface::ABSOLUTE_URL));

        $this->mailer->send($email);
    }

    public function sendResettingEmailMessage(UserInterface $user)
    {
        $email = (new Email())
            ->from('info@fungassist.net')
            ->to($user->getEmail())
            ->subject('Password Reset FungAssist')
            ->html('<p>Please follow this link to reset your password:</p>'. $this->router->generate('fos_user_resetting_reset', ['token'=>$user->getConfirmationToken()], UrlGeneratorInterface::ABSOLUTE_URL));

        $this->mailer->send($email);
    }

}