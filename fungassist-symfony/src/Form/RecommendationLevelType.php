<?php

namespace App\Form;

use App\Entity\RecommendationLevel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecommendationLevelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('specification')
            ->add('level')
            ->add('description')
            ->add('hierachy')
            ->add('guidelines');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RecommendationLevel::class,
        ]);
    }
}
