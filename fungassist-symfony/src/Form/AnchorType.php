<?php

namespace App\Form;

use App\Entity\Anchor;
use App\Entity\Guideline;
use App\Entity\QuestionItem;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnchorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('text', CKEditorType::class, ['config_name' => 'full'])
            ->add('context', CKEditorType::class, ['config_name' => 'full']);
//            ->add('evidenceLevel')
//            ->add('recommendationLevel')


        // if something was preset use the data
        if (array_key_exists('guidelines', $options['empty_data'])) {
            $builder->add('guideline', EntityType::class, [
                'class' => Guideline::class,
                'choices' => $options['empty_data']['guidelines'],
            ]);

        }

        if (array_key_exists('questionItem', $options['empty_data'])) {
            $builder->add('questionItem', EntityType::class, [
                'class' => QuestionItem::class,
                'choices' => $options['empty_data']['questionItem'],
            ]);
        }
//        else {
//            $builder->add('guideline')
//                ->add('questionItem');
//                ->add('algorithm');
//                ->add('guidelineTable')
//                ->add('guidelineFigures');
//        }


    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Anchor::class,
        ]);
    }
}
