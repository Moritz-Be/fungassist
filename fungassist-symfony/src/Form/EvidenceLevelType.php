<?php

namespace App\Form;

use App\Entity\EvidenceLevel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvidenceLevelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('specification')
            ->add('level')
            ->add('description')
            ->add('hierachy')
            ->add('guidelines');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EvidenceLevel::class,
        ]);
    }
}
