<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuidelineHistoryRepository")
 */
class GuidelineHistory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $authors;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="integer")
     */
    private $versionCount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Guideline", inversedBy="guidelineHistories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $guideline;

    /**
     * @ORM\Column(type="datetime")
     */
    private $revisionDate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthors(): ?string
    {
        return $this->authors;
    }

    public function setAuthors(string $authors): self
    {
        $this->authors = $authors;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getVersionCount(): ?int
    {
        return $this->versionCount;
    }

    public function setVersionCount(int $versionCount): self
    {
        $this->versionCount = $versionCount;

        return $this;
    }

    public function getGuideline(): ?Guideline
    {
        return $this->guideline;
    }

    public function setGuideline(?Guideline $guideline): self
    {
        $this->guideline = $guideline;

        return $this;
    }

    public function getRevisionDate(): ?DateTimeInterface
    {
        return $this->revisionDate;
    }

    public function setRevisionDate(DateTimeInterface $revisionDate): self
    {
        $this->revisionDate = $revisionDate;

        return $this;
    }
}
