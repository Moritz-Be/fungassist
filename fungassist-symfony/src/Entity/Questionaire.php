<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionaireRepository")
 */
class Questionaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $title;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Guideline", mappedBy="questionaire")
     */
    private $guidelines;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionItem", mappedBy="questionaire")
     */
    private $questionItems;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Algorithm", mappedBy="questionaire")
     */
    private $algorithms;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionaireResult", mappedBy="questionaire")
     */
    private $questionaireResults;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $caseReportTemmplate;


    public function __construct()
    {
        $this->questionItems = new ArrayCollection();
        $this->guidelines = new ArrayCollection();
        $this->algorithms = new ArrayCollection();
        $this->questionaireResults = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function __toString()
    {
        return $this->title;
    }

    /**
     * @return Collection|Guideline[]
     */
    public function getGuidelines(): ?Collection
    {
        return $this->guidelines;
    }

    /**
     * @return Collection|QuestionItem[]
     */
    public function getQuestionItems(): Collection
    {
        return $this->questionItems;
    }

    public function addQuestionItem(QuestionItem $questionItem): self
    {
        if (!$this->questionItems->contains($questionItem)) {
            $this->questionItems[] = $questionItem;
            $questionItem->setQuestionaire($this);
        }

        return $this;
    }

    public function removeQuestionItem(QuestionItem $questionItem): self
    {
        if ($this->questionItems->contains($questionItem)) {
            $this->questionItems->removeElement($questionItem);
            // set the owning side to null (unless already changed)
            if ($questionItem->getQuestionaire() === $this) {
                $questionItem->setQuestionaire(null);
            }
        }

        return $this;
    }

    /**
     * function to get direct access to the IDs of all guidelines of the questionaire
     * @return array
     */
    public function getGuidelineIds(){
        $guidelineIds = [];
        foreach ($this->getGuidelines() as $guideline) {
            $guidelineIds[] = $guideline->getId();
        }
        return $guidelineIds;
    }

    /**
     * @return Collection|Algorithm[]
     */
    public function getAlgorithms(): Collection
    {
        return $this->algorithms;
    }

    public function addAlgorithm(Algorithm $algorithm): self
    {
        if (!$this->algorithms->contains($algorithm)) {
            $this->algorithms[] = $algorithm;
            $algorithm->setQuestionaire($this);
        }

        return $this;
    }

    public function removeAlgorithm(Algorithm $algorithm): self
    {
        if ($this->algorithms->contains($algorithm)) {
            $this->algorithms->removeElement($algorithm);
            // set the owning side to null (unless already changed)
            if ($algorithm->getQuestionaire() === $this) {
                $algorithm->setQuestionaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|QuestionaireResult[]
     */
    public function getQuestionaireResults(): Collection
    {
        return $this->questionaireResults;
    }

    public function addQuestionaireResult(QuestionaireResult $questionaireResult): self
    {
        if (!$this->questionaireResults->contains($questionaireResult)) {
            $this->questionaireResults[] = $questionaireResult;
            $questionaireResult->setQuestionaire($this);
        }

        return $this;
    }

    public function removeQuestionaireResult(QuestionaireResult $questionaireResult): self
    {
        if ($this->questionaireResults->contains($questionaireResult)) {
            $this->questionaireResults->removeElement($questionaireResult);
            // set the owning side to null (unless already changed)
            if ($questionaireResult->getQuestionaire() === $this) {
                $questionaireResult->setQuestionaire(null);
            }
        }

        return $this;
    }

    public function getCaseReportTemmplate(): ?string
    {
        return $this->caseReportTemmplate;
    }

    public function setCaseReportTemmplate(?string $caseReportTemmplate): self
    {
        $this->caseReportTemmplate = $caseReportTemmplate;

        return $this;
    }

}
