<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuidelineSourceRepository")
 */
class GuidelineSource
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

//    /**
//     * @ORM\Column(type="text")
//     */
//    private $title;
//
//    /**
//     * @ORM\Column(type="text", nullable=true)
//     */
//    private $authors;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Guideline", inversedBy="guidelineSources")
     */
    private $guidelines;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quoteNumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $articleQuoteString;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pubmedId;

    public function __construct()
    {
        $this->guidelines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

//    public function getTitle(): ?string
//    {
//        return $this->title;
//    }
//
//    public function setTitle(string $title): self
//    {
//        $this->title = $title;
//
//        return $this;
//    }
//
//    public function getAuthors(): ?string
//    {
//        return $this->authors;
//    }
//
//    public function setAuthors(string $authors): self
//    {
//        $this->authors = $authors;
//
//        return $this;
//    }

    /**
     * @return Collection|Guideline[]
     */
    public function getGuidelines(): Collection
    {
        return $this->guidelines;
    }

    public function addGuideline(Guideline $guideline): self
    {
        if (!$this->guidelines->contains($guideline)) {
            $this->guidelines[] = $guideline;
        }

        return $this;
    }

    public function removeGuideline(Guideline $guideline): self
    {
        if ($this->guidelines->contains($guideline)) {
            $this->guidelines->removeElement($guideline);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getArticleQuoteString();
    }

    public function getQuoteNumber(): ?int
    {
        return $this->quoteNumber;
    }

    public function setQuoteNumber(?int $quoteNumber): self
    {
        $this->quoteNumber = $quoteNumber;

        return $this;
    }

    public function getArticleQuoteString(): ?string
    {
        return $this->articleQuoteString;
    }

    public function setArticleQuoteString(?string $articleQuoteString): self
    {
        $this->articleQuoteString = $articleQuoteString;

        return $this;
    }

    public function getPubmedId(): ?int
    {
        return $this->pubmedId;
    }

    public function setPubmedId(?int $pubmedId): self
    {
        $this->pubmedId = $pubmedId;

        return $this;
    }

}
