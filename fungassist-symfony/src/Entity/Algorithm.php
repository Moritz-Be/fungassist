<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AlgorithmRepository")
 */
class Algorithm
{
    const SUPPORTED_OPERATORS = [
        'AND' => 'and',
        'OR' => 'or',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $ifStatement;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anchor", mappedBy="algorithm")
     * @ORM\JoinColumn(nullable=true)
     */
    private $anchorThen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Questionaire", inversedBy="algorithms")
     * @ORM\JoinColumn(nullable=false)
     */
    private $questionaire;

    /**
     * @ORM\Column(type="string", length=4048, nullable=true)
     */
    private $recommendedPath;

    /**
     * set var to true if execution of $recommended Path is true
     * @var boolean
     */
    private $choseRecommendedPath;

    public function __construct()
    {
        $this->anchorThen = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIfStatement(): ?string
    {
        return $this->ifStatement;
    }

    public function setIfStatement(string $ifStatement): self
    {
        $this->ifStatement = $ifStatement;

        return $this;
    }

    public function getQuestionaire(): ?Questionaire
    {
        return $this->questionaire;
    }

    public function setQuestionaire(?Questionaire $questionaire): self
    {
        $this->questionaire = $questionaire;

        return $this;
    }


    /**
     * @return Collection|Anchor[]
     */
    public function getAnchorThen(): Collection
    {
        return $this->anchorThen;
    }

    public function addAnchorThen(Anchor $anchorThen): self
    {
        if (!$this->anchorThen->contains($anchorThen)) {
            $this->anchorThen[] = $anchorThen;
            $anchorThen->setAlgorithm($this);
        }

        return $this;
    }

    public function removeAnchorThen(Anchor $anchorThen): self
    {
        if ($this->anchorThen->contains($anchorThen)) {
            $this->anchorThen->removeElement($anchorThen);
            // set the owning side to null (unless already changed)
            if ($anchorThen->getAlgorithm() === $this) {
                $anchorThen->setAlgorithm(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getQuestionaire() . ': ' . $this->getDescription();
    }

    public function getRecommendedPath(): ?string
    {
        return $this->recommendedPath;
    }

    public function setRecommendedPath(?string $recommendedPath): self
    {
        $this->recommendedPath = $recommendedPath;

        return $this;
    }

    /**
     * @return string
     */
    public function getChoseRecommendedPath(): string
    {
        return $this->choseRecommendedPath;
    }

    /**
     * @param string $choseRecommendedPath
     */
    public function setChoseRecommendedPath(string $choseRecommendedPath): string
    {
        $this->choseRecommendedPath = $choseRecommendedPath;

        return $this;
    }


}
