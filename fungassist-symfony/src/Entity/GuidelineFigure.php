<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuidelineFigureRepository")
 */
class GuidelineFigure
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Guideline", inversedBy="guidelineFigures")
     * @ORM\JoinColumn(nullable=false)
     */
    private $guideline;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anchor", mappedBy="guidelineFigures")
     */
    private $anchor;

    public function __construct()
    {
        $this->anchor = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getGuideline(): ?Guideline
    {
        return $this->guideline;
    }

    public function setGuideline(?Guideline $guideline): self
    {
        $this->guideline = $guideline;

        return $this;
    }

    /**
     * @return Collection|Anchor[]
     */
    public function getAnchor(): Collection
    {
        return $this->anchor;
    }

    public function addAnchor(Anchor $anchor): self
    {
        if (!$this->anchor->contains($anchor)) {
            $this->anchor[] = $anchor;
            $anchor->setGuidelineFigures($this);
        }

        return $this;
    }

    public function removeAnchor(Anchor $anchor): self
    {
        if ($this->anchor->contains($anchor)) {
            $this->anchor->removeElement($anchor);
            // set the owning side to null (unless already changed)
            if ($anchor->getGuidelineFigures() === $this) {
                $anchor->setGuidelineFigures(null);
            }
        }

        return $this;
    }
}
