<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionaireResultRepository")
 * @UniqueEntity("slug")
 */
class QuestionaireResult
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\QuestionItem")
     */
    private $selectedQuestionItems;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Anchor", inversedBy="cmt")
     */
    private $anchorsDisplayed;

    /**
     * @ORM\Column(type="string", length=4086, nullable=true)
     */
    private $cmt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Questionaire", inversedBy="questionaireResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $questionaire;

    public function __construct()
    {
        $this->selectedQuestionItems = new ArrayCollection();
        $this->anchorsDisplayed = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|QuestionItem[]
     */
    public function getSelectedQuestionItems(): Collection
    {
        return $this->selectedQuestionItems;
    }

    public function addSelectedQuestionItem(QuestionItem $selectedQuestionItem): self
    {
        if (!$this->selectedQuestionItems->contains($selectedQuestionItem)) {
            $this->selectedQuestionItems[] = $selectedQuestionItem;
        }

        return $this;
    }

    public function removeSelectedQuestionItem(QuestionItem $selectedQuestionItem): self
    {
        if ($this->selectedQuestionItems->contains($selectedQuestionItem)) {
            $this->selectedQuestionItems->removeElement($selectedQuestionItem);
        }

        return $this;
    }

    /**
     * @return Collection|Anchor[]
     */
    public function getAnchorsDisplayed(): Collection
    {
        return $this->anchorsDisplayed;
    }

    public function addAnchorsDisplayed(Anchor $anchorsDisplayed): self
    {
        if (!$this->anchorsDisplayed->contains($anchorsDisplayed)) {
            $this->anchorsDisplayed[] = $anchorsDisplayed;
        }

        return $this;
    }

    public function removeAnchorsDisplayed(Anchor $anchorsDisplayed): self
    {
        if ($this->anchorsDisplayed->contains($anchorsDisplayed)) {
            $this->anchorsDisplayed->removeElement($anchorsDisplayed);
        }

        return $this;
    }

    public function getCmt(): ?string
    {
        return $this->cmt;
    }

    public function setCmt(?string $cmt): self
    {
        $this->cmt = $cmt;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getQuestionaire(): ?Questionaire
    {
        return $this->questionaire;
    }

    public function setQuestionaire(?Questionaire $questionaire): self
    {
        $this->questionaire = $questionaire;

        return $this;
    }
}
