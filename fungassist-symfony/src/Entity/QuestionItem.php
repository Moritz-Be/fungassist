<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionItemRepository")
 */
class QuestionItem
{
    const TYPE = [
        "PARENT" => "PARENT",
        "SINGLE_SELECT" => "SINGLE_SELECT",
        "MULTI_SELECT" => "MULTI_SELECT",
        "OPTION" => "OPTION",
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $parentId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anchor", mappedBy="questionItem")
     */
    private $anchors;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Questionaire", inversedBy="questionItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $questionaire;

    /**
     * describes the questionitem above this one for generating a structured tree of all items
     * @ORM\Column(type="integer")
     */
    private $itemAbove;

    /**
     * non-database property for setting
     * @var QuestionItem[]
     */
    private $children = [];

    /**
     * @ORM\Column(type="string", length=4096, nullable=true)
     */
    private $displayCondition;

    public function __construct()
    {
        $this->questionaire = new ArrayCollection();
        $this->anchors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(?int $parentId): self
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return Collection|Anchor[]
     */
    public function getAnchors(): Collection
    {
        return $this->anchors;
    }

    public function addAnchor(Anchor $anchor): self
    {
        if (!$this->anchors->contains($anchor)) {
            $this->anchors[] = $anchor;
            $anchor->setQuestionItem($this);
        }

        return $this;
    }

    public function removeAnchor(Anchor $anchor): self
    {
        if ($this->anchors->contains($anchor)) {
            $this->anchors->removeElement($anchor);
            // set the owning side to null (unless already changed)
            if ($anchor->getQuestionItem() === $this) {
                $anchor->setQuestionItem(null);
            }
        }

        return $this;
    }

    public function getQuestionaire()
    {
        return $this->questionaire;
    }

    public function setQuestionaire(?Questionaire $questionaire): self
    {
        $this->questionaire = $questionaire;

        return $this;
    }

    public function __toString()
    {
        return $this->getQuestionaire() . ': (' . $this->type . ') ' . $this->content;
    }

    public function getItemAbove(): ?int
    {
        return $this->itemAbove;
    }

    public function setItemAbove(int $itemAbove): self
    {
        $this->itemAbove = $itemAbove;

        return $this;
    }

    /**
     * @return QuestionItem[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param QuestionItem[] $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }

    public function addChild(QuestionItem $questionItem)
    {
        $this->children[] = $questionItem;
    }

    public function getDisplayCondition(): ?string
    {
        return $this->displayCondition;
    }

    public function setDisplayCondition(?string $displayCondition): self
    {
        $displayCondition = $displayCondition == '' ? null : $displayCondition;

        $this->displayCondition = $displayCondition;

        return $this;
    }


}
