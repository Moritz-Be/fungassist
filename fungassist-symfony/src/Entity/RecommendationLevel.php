<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RecommendationLevelRepository")
 */
class RecommendationLevel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $specification;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $level;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $hierachy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anchor", mappedBy="recommendationLevel")
     */
    private $anchors;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Guideline", inversedBy="recommendationLevels")
     */
    private $guidelines;

    public function __construct()
    {
        $this->anchors = new ArrayCollection();
        $this->guidelines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecification(): ?string
    {
        return $this->specification;
    }

    public function setSpecification(string $specification): self
    {
        $this->specification = $specification;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getHierachy(): ?int
    {
        return $this->hierachy;
    }

    public function setHierachy(int $hierachy): self
    {
        $this->hierachy = $hierachy;

        return $this;
    }

    /**
     * @return Collection|Anchor[]
     */
    public function getAnchors(): Collection
    {
        return $this->anchors;
    }

    public function addAnchor(Anchor $anchor): self
    {
        if (!$this->anchors->contains($anchor)) {
            $this->anchors[] = $anchor;
            $anchor->setRecommendationLevel($this);
        }

        return $this;
    }

    public function removeAnchor(Anchor $anchor): self
    {
        if ($this->anchors->contains($anchor)) {
            $this->anchors->removeElement($anchor);
            // set the owning side to null (unless already changed)
            if ($anchor->getRecommendationLevel() === $this) {
                $anchor->setRecommendationLevel(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Guideline[]
     */
    public function getGuidelines(): Collection
    {
        return $this->guidelines;
    }

    public function addGuideline(Guideline $guideline): self
    {
        if (!$this->guidelines->contains($guideline)) {
            $this->guidelines[] = $guideline;
        }

        return $this;
    }

    public function removeGuideline(Guideline $guideline): self
    {
        if ($this->guidelines->contains($guideline)) {
            $this->guidelines->removeElement($guideline);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->specification . ': ' . $this->level . ' (' . $this->description . ')';
    }
}
