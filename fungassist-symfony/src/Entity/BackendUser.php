<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class BackendUser extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     *
     * @Assert\Length(
     *     min=12,
     *     max=4096,
     *     minMessage="fos_user.password.short",
     *     groups={"Profile", "ResetPassword", "Registration", "ChangePassword"}
     * )
     *
     * @Assert\NotCompromisedPassword
     *
     */
    protected $plainPassword;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anchor", mappedBy="reviewBy")
     */
    private $reviewedAnchors;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->reviewedAnchors = new ArrayCollection();
    }



    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        $this->setUsername($email);
        $this->email = $email;

        return $this;
    }


    /**
     * @return Collection|Anchor[]
     */
    public function getReviewedAnchors(): Collection
    {
        return $this->reviewedAnchors;
    }

    public function addReviewedAnchor(Anchor $reviewedAnchor): self
    {
        if (!$this->reviewedAnchors->contains($reviewedAnchor)) {
            $this->reviewedAnchors[] = $reviewedAnchor;
            $reviewedAnchor->setReviewBy($this);
        }

        return $this;
    }

    public function removeReviewedAnchor(Anchor $reviewedAnchor): self
    {
        if ($this->reviewedAnchors->contains($reviewedAnchor)) {
            $this->reviewedAnchors->removeElement($reviewedAnchor);
            // set the owning side to null (unless already changed)
            if ($reviewedAnchor->getReviewBy() === $this) {
                $reviewedAnchor->setReviewBy(null);
            }
        }

        return $this;
    }
}