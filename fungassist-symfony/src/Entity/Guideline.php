<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GuidelineRepository")
 */
class Guideline
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $authors;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * Text field with formatted anchors
     * @var string
     */
    private $formattedText;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Questionaire", inversedBy="guidelines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $questionaire;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Anchor", mappedBy="guideline")
     */
    private $anchors;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\RecommendationLevel", mappedBy="guidelines")
     */
    private $recommendationLevels;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\GuidelineSource", mappedBy="guidelines")
     */
    private $guidelineSources;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\EvidenceLevel", mappedBy="guidelines")
     */
    private $evidenceLevels;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GuidelineHistory", mappedBy="guideline")
     */
    private $guidelineHistories;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $organization;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $abstract;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $appendix;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $acknowledgements;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publishedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publishedBy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $publishedIn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $doi;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $conflictsOfInterest;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $sponsoring;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cmt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GuidelineTable", mappedBy="guideline")
     */
    private $guidelineTables;

    /**
     * @ORM\OneToMany(targetEntity="GuidelineFigure", mappedBy="guideline")
     */
    private $guidelineFigures;


    public function __construct()
    {
        $this->questionaire = new ArrayCollection();
        $this->anchors = new ArrayCollection();
        $this->recommendationLevels = new ArrayCollection();
        $this->guidelineSources = new ArrayCollection();
        $this->evidenceLevels = new ArrayCollection();
        $this->guidelineHistories = new ArrayCollection();
        $this->guidelineTables = new ArrayCollection();
        $this->guidelineFigures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthors(): ?string
    {
        return $this->authors;
    }

    public function getAuthorsShort(): ?string
    {
        // remove everything after the first comma and add et al
        $authors = preg_replace('/,.*/','',$this->authors);

        return $authors . ' et al';
    }

    public function setAuthors(string $authors): self
    {
        $this->authors = $authors;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = trim(str_replace("\xC2\xA0", ' ', html_entity_decode($text, null, 'UTF-8')));

        return $this;
    }

    public function getQuestionaire(): ?Questionaire
    {
        return $this->questionaire;
    }

    public function setQuestionaire(?Questionaire $questionaire): self
    {
        $this->questionaire = $questionaire;

        return $this;
    }

    /**
     * @return Collection|Anchor[]
     */
    public function getAnchors(): Collection
    {
        return $this->anchors;
    }

    public function addAnchor(Anchor $anchor): self
    {
        if (!$this->anchors->contains($anchor)) {
            $this->anchors[] = $anchor;
            $anchor->setGuideline($this);
        }

        return $this;
    }

    public function removeAnchor(Anchor $anchor): self
    {
        if ($this->anchors->contains($anchor)) {
            $this->anchors->removeElement($anchor);
            // set the owning side to null (unless already changed)
            if ($anchor->getGuideline() === $this) {
                $anchor->setGuideline(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RecommendationLevel[]
     */
    public function getRecommendationLevels(): Collection
    {
        return $this->recommendationLevels;
    }

    public function addRecommendationLevel(RecommendationLevel $recommendationLevel): self
    {
        if (!$this->recommendationLevels->contains($recommendationLevel)) {
            $this->recommendationLevels[] = $recommendationLevel;
            $recommendationLevel->addGuideline($this);
        }

        return $this;
    }

    public function removeRecommendationLevel(RecommendationLevel $recommendationLevel): self
    {
        if ($this->recommendationLevels->contains($recommendationLevel)) {
            $this->recommendationLevels->removeElement($recommendationLevel);
            $recommendationLevel->removeGuideline($this);
        }

        return $this;
    }

    /**
     * @return Collection|GuidelineSource[]
     */
    public function getGuidelineSources(): Collection
    {
        return $this->guidelineSources;
    }

    public function addGuidelineSource(GuidelineSource $guidelineSource): self
    {
        if (!$this->guidelineSources->contains($guidelineSource)) {
            $this->guidelineSources[] = $guidelineSource;
            $guidelineSource->addGuideline($this);
        }

        return $this;
    }

    public function removeGuidelineSource(GuidelineSource $guidelineSource): self
    {
        if ($this->guidelineSources->contains($guidelineSource)) {
            $this->guidelineSources->removeElement($guidelineSource);
            $guidelineSource->removeGuideline($this);
        }

        return $this;
    }

    /**
     * @return Collection|EvidenceLevel[]
     */
    public function getEvidenceLevels(): Collection
    {
        return $this->evidenceLevels;
    }

    public function addEvidenceLevel(EvidenceLevel $evidenceLevel): self
    {
        if (!$this->evidenceLevels->contains($evidenceLevel)) {
            $this->evidenceLevels[] = $evidenceLevel;
            $evidenceLevel->addGuideline($this);
        }

        return $this;
    }

    public function removeEvidenceLevel(EvidenceLevel $evidenceLevel): self
    {
        if ($this->evidenceLevels->contains($evidenceLevel)) {
            $this->evidenceLevels->removeElement($evidenceLevel);
            $evidenceLevel->removeGuideline($this);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->title.', '.$this->getAuthorsShort().', '.date_format($this->getPublishedAt(),"Y");
    }

    public function getOrganization(): ?string
    {
        return $this->organization;
    }

    public function setOrganization(?string $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getAbstract(): ?string
    {
        return $this->abstract;
    }

    public function setAbstract(?string $abstract): self
    {
        $this->abstract = $abstract;

        return $this;
    }

    public function getAppendix(): ?string
    {
        return $this->appendix;
    }

    public function setAppendix(?string $appendix): self
    {
        $this->appendix = $appendix;

        return $this;
    }

    public function getAcknowledgements(): ?string
    {
        return $this->acknowledgements;
    }

    public function setAcknowledgements(?string $acknowledgements): self
    {
        $this->acknowledgements = $acknowledgements;

        return $this;
    }

    public function getPublishedAt(): ?DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(DateTimeInterface $publishedAt): self
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getPublishedBy(): ?string
    {
        return $this->publishedBy;
    }

    public function setPublishedBy(string $publishedBy): self
    {
        $this->publishedBy = $publishedBy;

        return $this;
    }

    public function getPublishedIn(): ?string
    {
        return $this->publishedIn;
    }

    public function setPublishedIn(string $publishedIn): self
    {
        $this->publishedIn = $publishedIn;

        return $this;
    }

    public function getDoi(): ?string
    {
        return $this->doi;
    }

    public function setDoi(string $doi): self
    {
        $this->doi = $doi;

        return $this;
    }

    public function getConflictsOfInterest(): ?string
    {
        return $this->conflictsOfInterest;
    }

    public function setConflictsOfInterest(?string $conflictsOfInterest): self
    {
        $this->conflictsOfInterest = $conflictsOfInterest;

        return $this;
    }

    public function getSponsoring(): ?string
    {
        return $this->sponsoring;
    }

    public function setSponsoring(?string $sponsoring): self
    {
        $this->sponsoring = $sponsoring;

        return $this;
    }

    public function getCmt(): ?string
    {
        return $this->cmt;
    }

    public function setCmt(?string $cmt): self
    {
        $this->cmt = $cmt;

        return $this;
    }

    /**
     * @return Collection|GuidelineTable[]
     */
    public function getGuidelineTables(): Collection
    {
        return $this->guidelineTables;
    }

    public function addGuidelineTable(GuidelineTable $guidelineTable): self
    {
        if (!$this->guidelineTables->contains($guidelineTable)) {
            $this->guidelineTables[] = $guidelineTable;
            $guidelineTable->setGuideline($this);
        }

        return $this;
    }

    public function removeGuidelineTable(GuidelineTable $guidelineTable): self
    {
        if ($this->guidelineTables->contains($guidelineTable)) {
            $this->guidelineTables->removeElement($guidelineTable);
            // set the owning side to null (unless already changed)
            if ($guidelineTable->getGuideline() === $this) {
                $guidelineTable->setGuideline(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GuidelineFigure[]
     */
    public function getGuidelineFigures(): Collection
    {
        return $this->guidelineFigures;
    }

    public function addGuidelineFigure(GuidelineFigure $guidelineFigure): self
    {
        if (!$this->guidelineFigures->contains($guidelineFigure)) {
            $this->guidelineFigures[] = $guidelineFigure;
            $guidelineFigure->setGuideline($this);
        }

        return $this;
    }

    public function removeGuidelineFigure(GuidelineFigure $guidelineFigure): self
    {
        if ($this->guidelineFigures->contains($guidelineFigure)) {
            $this->guidelineFigures->removeElement($guidelineFigure);
            // set the owning side to null (unless already changed)
            if ($guidelineFigure->getGuideline() === $this) {
                $guidelineFigure->setGuideline(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GuidelineHistory[]
     */
    public function getGuidelineHistories(): Collection
    {
        return $this->guidelineHistories;
    }

    public function addGuidelineHistory(GuidelineHistory $guidelineHistory): self
    {
        if (!$this->guidelineHistories->contains($guidelineHistory)) {
            $this->guidelineHistories[] = $guidelineHistory;
            $guidelineHistory->setGuideline($this);
        }

        return $this;
    }

    public function removeGuidelineHistory(GuidelineHistory $guidelineHistory): self
    {
        if ($this->guidelineHistories->contains($guidelineHistory)) {
            $this->guidelineHistories->removeElement($guidelineHistory);
            // set the owning side to null (unless already changed)
            if ($guidelineHistory->getGuideline() === $this) {
                $guidelineHistory->setGuideline(null);
            }
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFormattedText()
    {
        return $this->formattedText;
    }

    /**
     * @param $formattedText
     * @return Guideline
     */
    public function setFormattedText($formattedText): self
    {
        $this->formattedText = $formattedText;

        return $this;
    }


}
