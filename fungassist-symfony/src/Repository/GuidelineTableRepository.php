<?php

namespace App\Repository;

use App\Entity\GuidelineTable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GuidelineTable|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuidelineTable|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuidelineTable[]    findAll()
 * @method GuidelineTable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuidelineTableRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GuidelineTable::class);
    }

    // /**
    //  * @return GuidelineTable[] Returns an array of GuidelineTable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GuidelineTable
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
