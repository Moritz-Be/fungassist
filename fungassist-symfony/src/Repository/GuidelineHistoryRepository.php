<?php

namespace App\Repository;

use App\Entity\Guideline;
use App\Entity\GuidelineHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GuidelineHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuidelineHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuidelineHistory[]    findAll()
 * @method GuidelineHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuidelineHistoryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GuidelineHistory::class);
    }

    // /**
    //  * @return GuidelineHistory[] Returns an array of GuidelineHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findMaxVersion(Guideline $guideline)
    {
        return $this->createQueryBuilder('g')
            ->select('g.versionCount')
            ->andWhere('g.guideline = :val')
            //->groupBy('g.guideline')
            ->setParameter('val', $guideline->getId())
            ->setMaxResults(1)
            ->orderBy('g.versionCount', 'DESC')
            ->getQuery()
            ->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR);
    }

}
