<?php

namespace App\Repository;

use App\Entity\EvidenceLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvidenceLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvidenceLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvidenceLevel[]    findAll()
 * @method EvidenceLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvidenceLevelRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvidenceLevel::class);
    }

    // /**
    //  * @return EvidenceLevel[] Returns an array of EvidenceLevel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EvidenceLevel
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
