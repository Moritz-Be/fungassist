<?php

namespace App\Repository;

use App\Entity\Guideline;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Guideline|null find($id, $lockMode = null, $lockVersion = null)
 * @method Guideline|null findOneBy(array $criteria, array $orderBy = null)
 * @method Guideline[]    findAll()
 * @method Guideline[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuidelineRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Guideline::class);
    }

    // /**
    //  * @return Guidelines[] Returns an array of Guidelines objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Guidelines
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
