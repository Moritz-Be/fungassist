<?php

namespace App\Repository;

use App\Entity\GuidelineFigure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GuidelineFigure|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuidelineFigure|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuidelineFigure[]    findAll()
 * @method GuidelineFigure[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuidelineFigureRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GuidelineFigure::class);
    }

    // /**
    //  * @return GuidelineFigures[] Returns an array of GuidelineFigures objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GuidelineFigures
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
