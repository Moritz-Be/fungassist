<?php

namespace App\Repository;

use App\Entity\RecommendationLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RecommendationLevel|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecommendationLevel|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecommendationLevel[]    findAll()
 * @method RecommendationLevel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecommendationLevelRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RecommendationLevel::class);
    }

    // /**
    //  * @return RecommendationLevel[] Returns an array of RecommendationLevel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RecommendationLevel
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
