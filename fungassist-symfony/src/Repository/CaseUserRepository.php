<?php

namespace App\Repository;

use App\Entity\CaseUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CaseUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method CaseUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method CaseUser[]    findAll()
 * @method CaseUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaseUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CaseUser::class);
    }

    // /**
    //  * @return CaseUser[] Returns an array of CaseUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CaseUser
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
