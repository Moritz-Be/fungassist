<?php

namespace App\Repository;

use App\Entity\Anchor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Anchor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Anchor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Anchor[]    findAll()
 * @method Anchor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnchorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Anchor::class);
    }

    // /**
    //  * @return Anchor[] Returns an array of Anchor objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Anchor
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
