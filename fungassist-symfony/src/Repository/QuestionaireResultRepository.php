<?php

namespace App\Repository;

use App\Entity\Questionaire;
use App\Entity\QuestionaireResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method QuestionaireResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionaireResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionaireResult[]    findAll()
 * @method QuestionaireResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionaireResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuestionaireResult::class);
    }

    // /**
    //  * @return QuestionaireResults[] Returns an array of QuestionaireResults objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function resultsPastMonth(Questionaire $questionaire)
    {
        $lastMonth = new \DateTime('now');
        $lastMonth->modify('-30 days');

        return $this->createQueryBuilder('q')
            ->andWhere('q.createdAt BETWEEN :lastMonth AND :now')
            ->andWhere('q.questionaire = :questionaire')
            ->andWhere('q.cmt = :finalSubmit')
            ->setParameter('now', new \DateTime('now'))
            ->setParameter('lastMonth', $lastMonth)
            ->setParameter('finalSubmit', 'finalSubmit')
            ->setParameter('questionaire', $questionaire)
            ->getQuery()
            ->getResult()
        ;
    }

}
