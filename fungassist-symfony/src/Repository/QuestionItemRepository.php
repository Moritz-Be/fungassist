<?php

namespace App\Repository;

use App\Entity\QuestionItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\SimpleArrayType;
use Doctrine\ORM\AbstractQuery;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuestionItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionItem[]    findAll()
 * @method QuestionItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuestionItem::class);
    }


    public function findLowestChild($parentId){
        return $this->createQueryBuilder('q')
            ->select('q.id')
            ->andWhere('q.parentId = :val')
            ->setParameter('val', $parentId)
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
            ->getQuery()
            ->getScalarResult()
            ;
    }

    public function getPossibleQuestionItemParents($questionaireId){
        $results = $this->createQueryBuilder('q')
            ->select('q.id', 'q.content', 'q.type')
            ->andWhere('q.questionaire = :val')
            ->andWhere('q.type  IN(:option)')
            ->setParameter('val', $questionaireId)
            ->setParameter('option', [QuestionItem::TYPE['SINGLE_SELECT'], QuestionItem::TYPE['MULTI_SELECT']])
//            ->orderBy('t.id', 'ASC')
//            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
            ;

        $arrayToReturn = [];
        foreach($results as $result){
            $arrayToReturn[$result['id']]= $result['content'].' ('.$result['type'].')';
        }

        return $arrayToReturn;
    }



    // /**
    //  * @return QuestionItem[] Returns an array of QuestionItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionItem
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
