<?php

namespace App\Controller;

use App\Entity\Anchor;
use App\Repository\QuestionaireRepository;
use App\Repository\QuestionItemRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("backend/review")
 */
class ReviewController extends AbstractController
{

    /**
     * @Route("/", name="review_index")
     */
    public function index(QuestionaireRepository $questionaireRepository): Response
    {
        return $this->render('review/index.html.twig', [
            'questionaires' => $questionaireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/questionItem/{questionItemId}/anchors", name="review_anchor_by_questionitem_index")
     */
    public function indexAnchorByQuestionItemReview(int $questionItemId, QuestionItemRepository $questionItemRepository)
    {
        $questionItem = $questionItemRepository->findOneBy(['id' => $questionItemId]);
        $parentQuestionItem = $questionItemRepository->findOneBy(['id' => $questionItem->getParentId()]);

        return $this->render('review/index_anchors_by_questionitem.html.twig', [
            'questionItem' => $questionItem,
            'questionItemParent' => $parentQuestionItem
        ]);
    }

    /**
     * @Route("/questionaire/{questionaireId}", name="review_anchor_by_questionaire_index")
     */
    public function indexAnchorByQuestionaireReview(int $questionaireId, QuestionItemRepository $questionItemRepository)
    {
        $questionItems = $questionItemRepository->findBy(['questionaire' => $questionaireId]);

        return $this->render('review/index_anchors_by_questionaire.html.twig', [
            'questionItems' => $questionItems,
        ]);
    }

    /**
     * @Route("/anchor-ok/{id}/{questionaireId}", name="review_anchor_approve")
     */
    public function approveAnchor(Request $request, Anchor $anchor)
    {
        $anchor->setReviewStatus(Anchor::ANCHOR_REVIEW_STATUS['OK']);
        $anchor->setReviewAt(new DateTime('now'));
        $anchor->setReviewCmt('approved');
        $anchor->setReviewBy($this->getUser());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($anchor);
        $entityManager->flush();
        return $this->redirect($request->headers->get('referer'));
    }

}
