<?php

namespace App\Controller;

use App\Entity\EvidenceLevel;
use App\Form\EvidenceLevelType;
use App\Repository\EvidenceLevelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backendevidencelevel")
 */
class EvidenceLevelController extends AbstractController
{
    /**
     * @Route("/", name="evidence_level_index", methods={"GET"})
     */
    public function index(EvidenceLevelRepository $evidenceLevelRepository): Response
    {
        return $this->render('evidence_level/index.html.twig', [
            'evidence_levels' => $evidenceLevelRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="evidence_level_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $evidenceLevel = new EvidenceLevel();
        $form = $this->createForm(EvidenceLevelType::class, $evidenceLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($evidenceLevel);
            $entityManager->flush();

            return $this->redirectToRoute('evidence_level_index');
        }

        return $this->render('evidence_level/new.html.twig', [
            'evidence_level' => $evidenceLevel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evidence_level_show", methods={"GET"})
     */
    public function show(EvidenceLevel $evidenceLevel): Response
    {
        return $this->render('evidence_level/show.html.twig', [
            'evidence_level' => $evidenceLevel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="evidence_level_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, EvidenceLevel $evidenceLevel): Response
    {
        $form = $this->createForm(EvidenceLevelType::class, $evidenceLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('evidence_level_index');
        }

        return $this->render('evidence_level/edit.html.twig', [
            'evidence_level' => $evidenceLevel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="evidence_level_delete", methods={"DELETE"})
     */
    public function delete(Request $request, EvidenceLevel $evidenceLevel): Response
    {
        if ($this->isCsrfTokenValid('delete' . $evidenceLevel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($evidenceLevel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('evidence_level_index');
    }
}
