<?php

namespace App\Controller;

use App\Entity\Guideline;
use App\Form\GuidelineType;
use App\Repository\GuidelineRepository;
use App\Repository\QuestionaireRepository;
use App\Service\GuidelineHistoryService;
use App\Service\GuidelineService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/guideline")
 */
class GuidelineController extends AbstractController
{
    /**
     * @Route("/", name="guideline_index", methods={"GET"})
     */
    public function index(QuestionaireRepository $questionaireRepository): Response
    {
        return $this->render('guideline/index.html.twig', [
            'questionaires' => $questionaireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/questionaire/{questionaireId}", name="guideline_new", methods={"GET","POST"})
     */
    public function new(int $questionaireId, Request $request, QuestionaireRepository $questionaireRepository): Response
    {
        $questionaire = $questionaireRepository->findOneBy(['id'=>$questionaireId]);

        $guideline = new Guideline();
        $guideline->setQuestionaire($questionaire);

        $form = $this->createForm(GuidelineType::class, $guideline);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($guideline);
            $entityManager->flush();

            return $this->redirectToRoute('guideline_index');
        }

        return $this->render('guideline/new.html.twig', [
            'questionaire' => $questionaire,
            'guideline' => $guideline,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guideline_show", methods={"GET"})
     */
    public function show(Guideline $guideline, GuidelineService $guidelineService): Response
    {
        $guideline = $guidelineService->formatAnchors($guideline);
        return $this->render('guideline/show.html.twig', [
            'guideline' => $guideline,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="guideline_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Guideline $guideline, GuidelineHistoryService $guidelineHistoryService): Response
    {
        $form = $this->createForm(GuidelineType::class, $guideline);
        $guidelineSources = $guideline->getGuidelineSources();

        $diffToGuidelineHistories = $guidelineHistoryService->diffToHistory($guideline);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $guidelineHistoryService->updateGuideline($guideline);

            return $this->redirectToRoute('guideline_index');
        }

        return $this->render('guideline/edit.html.twig', [
            'guideline' => $guideline,
            'form' => $form->createView(),
            'guidelineSources' => $guidelineSources,
            'diffToGuidelineHistories' => $diffToGuidelineHistories,
        ]);
    }

    /**
     * @Route("/{id}", name="guideline_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Guideline $guideline): Response
    {
        if ($this->isCsrfTokenValid('delete' . $guideline->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($guideline);
            $entityManager->flush();
        }

        return $this->redirectToRoute('guideline_index');
    }
}
