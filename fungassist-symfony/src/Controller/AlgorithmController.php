<?php

namespace App\Controller;

use App\Entity\Algorithm;
use App\Form\AlgorithmType;
use App\Repository\AlgorithmRepository;
use App\Repository\QuestionaireRepository;
use App\Repository\QuestionItemRepository;
use App\Service\AlgorithmService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/algorithm")
 */
class AlgorithmController extends AbstractController
{
    /**
     * @Route("/", name="algorithm_index", methods={"GET"})
     */
    public function index(QuestionaireRepository $questionaireRepository): Response
    {
        return $this->render('algorithm/index.html.twig', [
            'questionaires' => $questionaireRepository->findAll(),
        ]);
    }

    /**
     *  Find all question items for a given questionaireId
     * @Route("/questionaire/{questionaireId}", name="algorithm_index_by_questionaire", methods={"GET"})
     */
    public function indexByQuestionaire(int $questionaireId, QuestionaireRepository $questionaireRepository, AlgorithmRepository $algorithmRepository, AlgorithmService $algorithmService): Response
    {
        $questionaire = $questionaireRepository->findOneBy(['id' => $questionaireId]);

        $algorithms = $algorithmRepository->findBy(['questionaire' => $questionaire]);

        return $this->render('algorithm/index_by_questionaire.html.twig', [
            'questionaire' => $questionaire,
            'readableIfStatements' => $algorithmService->getReadableIfStatementsArray($algorithms),

        ]);
    }

    /**
     * @Route("/new/{questionaireId}", name="algorithm_new", methods={"GET","POST"})
     */
    public function new($questionaireId, Request $request, QuestionaireRepository $questionaireRepository, QuestionItemRepository $questionItemRepository): Response
    {
        $algorithm = new Algorithm();

        $questionaire = $questionaireRepository->findOneBy(['id' => $questionaireId]);

        $algorithm->setQuestionaire($questionaire);
        $form = $this->createForm(AlgorithmType::class, $algorithm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($algorithm);
            $entityManager->flush();

            return $this->redirectToRoute('anchor_algorithm_new', ['algorithmId' => $algorithm->getId()]);
        }

        return $this->render('algorithm/new.html.twig', [
            'algorithm' => $algorithm,
            'questionItemParents' => $questionItemRepository->getPossibleQuestionItemParents($questionaireId),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="algorithm_show", methods={"GET"})
     */
    public function show(Algorithm $algorithm, AlgorithmService $algorithmService): Response
    {
        return $this->render('algorithm/show.html.twig', [
            'readableIfStatement' => $algorithmService->generateReadableIfStatement($algorithm),
            'algorithm' => $algorithm,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="algorithm_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Algorithm $algorithm, AlgorithmService $algorithmService, QuestionItemRepository $questionItemRepository): Response
    {
        $form = $this->createForm(AlgorithmType::class, $algorithm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('algorithm_edit', ['id' => $algorithm->getId()]);
        }

        return $this->render('algorithm/edit.html.twig', [
            'algorithm' => $algorithm,
            'readableIfStatement' => $algorithmService->generateReadableIfStatement($algorithm),
            'questionItemParents' => $questionItemRepository->getPossibleQuestionItemParents($algorithm->getQuestionaire()->getId()),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="algorithm_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Algorithm $algorithm): Response
    {
        if ($this->isCsrfTokenValid('delete' . $algorithm->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($algorithm);
            $entityManager->flush();
        }

        return $this->redirectToRoute('algorithm_index');
    }
}
