<?php

namespace App\Controller;

use App\Entity\RecommendationLevel;
use App\Form\RecommendationLevelType;
use App\Repository\RecommendationLevelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/recommendationlevel")
 */
class RecommendationLevelController extends AbstractController
{
    /**
     * @Route("/", name="recommendation_level_index", methods={"GET"})
     */
    public function index(RecommendationLevelRepository $recommendationLevelRepository): Response
    {
        return $this->render('recommendation_level/index.html.twig', [
            'recommendation_levels' => $recommendationLevelRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="recommendation_level_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $recommendationLevel = new RecommendationLevel();
        $form = $this->createForm(RecommendationLevelType::class, $recommendationLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($recommendationLevel);
            $entityManager->flush();

            return $this->redirectToRoute('recommendation_level_index');
        }

        return $this->render('recommendation_level/new.html.twig', [
            'recommendation_level' => $recommendationLevel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="recommendation_level_show", methods={"GET"})
     */
    public function show(RecommendationLevel $recommendationLevel): Response
    {
        return $this->render('recommendation_level/show.html.twig', [
            'recommendation_level' => $recommendationLevel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="recommendation_level_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RecommendationLevel $recommendationLevel): Response
    {
        $form = $this->createForm(RecommendationLevelType::class, $recommendationLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('recommendation_level_index');
        }

        return $this->render('recommendation_level/edit.html.twig', [
            'recommendation_level' => $recommendationLevel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="recommendation_level_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RecommendationLevel $recommendationLevel): Response
    {
        if ($this->isCsrfTokenValid('delete' . $recommendationLevel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($recommendationLevel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('recommendation_level_index');
    }
}
