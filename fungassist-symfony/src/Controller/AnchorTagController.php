<?php

namespace App\Controller;

use App\Entity\AnchorTag;
use App\Form\AnchorTagType;
use App\Repository\AnchorTagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/anchortag")
 */
class AnchorTagController extends AbstractController
{
    /**
     * @Route("/", name="anchor_tag_index", methods={"GET"})
     */
    public function index(AnchorTagRepository $anchorTagRepository): Response
    {
        return $this->render('anchor_tag/index.html.twig', [
            'anchor_tags' => $anchorTagRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="anchor_tag_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $anchorTag = new AnchorTag();
        $form = $this->createForm(AnchorTagType::class, $anchorTag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($anchorTag);
            $entityManager->flush();

            return $this->redirectToRoute('anchor_tag_index');
        }

        return $this->render('anchor_tag/new.html.twig', [
            'anchor_tag' => $anchorTag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="anchor_tag_show", methods={"GET"})
     */
    public function show(AnchorTag $anchorTag): Response
    {
        return $this->render('anchor_tag/show.html.twig', [
            'anchor_tag' => $anchorTag,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="anchor_tag_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AnchorTag $anchorTag): Response
    {
        $form = $this->createForm(AnchorTagType::class, $anchorTag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('anchor_tag_index');
        }

        return $this->render('anchor_tag/edit.html.twig', [
            'anchor_tag' => $anchorTag,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="anchor_tag_delete", methods={"DELETE"})
     */
    public function delete(Request $request, AnchorTag $anchorTag): Response
    {
        if ($this->isCsrfTokenValid('delete' . $anchorTag->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($anchorTag);
            $entityManager->flush();
        }

        return $this->redirectToRoute('anchor_tag_index');
    }
}
