<?php

namespace App\Controller;

use App\Entity\GuidelineTable;
use App\Form\GuidelineTableType;
use App\Repository\GuidelineTableRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/guidelinetable")
 */
class GuidelineTableController extends AbstractController
{
    /**
     * @Route("/", name="guideline_table_index", methods={"GET"})
     */
    public function index(GuidelineTableRepository $guidelineTableRepository): Response
    {
        return $this->render('guideline_table/index.html.twig', [
            'guideline_tables' => $guidelineTableRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="guideline_table_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $guidelineTable = new GuidelineTable();
        $form = $this->createForm(GuidelineTableType::class, $guidelineTable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($guidelineTable);
            $entityManager->flush();

            return $this->redirectToRoute('guideline_table_index');
        }

        return $this->render('guideline_table/new.html.twig', [
            'guideline_table' => $guidelineTable,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guideline_table_show", methods={"GET"})
     */
    public function show(GuidelineTable $guidelineTable): Response
    {
        return $this->render('guideline_table/show.html.twig', [
            'guideline_table' => $guidelineTable,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="guideline_table_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, GuidelineTable $guidelineTable): Response
    {
        $form = $this->createForm(GuidelineTableType::class, $guidelineTable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('guideline_table_index');
        }

        return $this->render('guideline_table/edit.html.twig', [
            'guideline_table' => $guidelineTable,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guideline_table_delete", methods={"DELETE"})
     */
    public function delete(Request $request, GuidelineTable $guidelineTable): Response
    {
        if ($this->isCsrfTokenValid('delete' . $guidelineTable->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($guidelineTable);
            $entityManager->flush();
        }

        return $this->redirectToRoute('guideline_table_index');
    }
}
