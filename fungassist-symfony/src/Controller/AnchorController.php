<?php

namespace App\Controller;

use App\Entity\Anchor;
use App\Form\AnchorType;
use App\Repository\AlgorithmRepository;
use App\Repository\AnchorRepository;
use App\Repository\GuidelineRepository;
use App\Repository\QuestionItemRepository;
use App\Service\AnchorService;
use App\Service\GuidelineService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/anchor")
 */
class AnchorController extends AbstractController
{
    /**
     * @Route("/", name="anchor_index", methods={"GET"})
     */
    public function index(AnchorRepository $anchorRepository): Response
    {
        return $this->render('anchor/index.html.twig', [
            'anchors' => $anchorRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/questionitem/{questionItemId}", name="anchor_questionitem_new", requirements={"questionItemId"="\d+"}, methods={"GET","POST"})
     */
    public function newWithRelToQuestionItem(Request $request, GuidelineService $guidelineService, QuestionItemRepository $questionItemRepository, GuidelineRepository $guidelineRepository, $questionItemId = null): Response
    {
        $anchor = new Anchor();
        if ($questionItemId != null) {

            // in case user came here with a guideline in mind preset the form
            $questionItem = $questionItemRepository->findOneBy(['id' => intval($questionItemId)]);
            $quesitonaireId = $questionItem->getQuestionaire()->getId();
            $guidelines = $guidelineRepository->findBy(['questionaire' => $quesitonaireId]);
            $anchor->setQuestionItem($questionItem);
            $form = $this->createForm(AnchorType::class, $anchor, ['empty_data' => ['questionItem' => [$questionItem], 'guidelines' => $guidelines]]);
            $form->handleRequest($request);
        } else {
            $form = $this->createForm(AnchorType::class, $anchor);
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($anchor);
            $entityManager->flush();
            $guidelineService->addAnchorToGuideline($anchor);

            // let user create multiple anchors in one session if he is comming from a questionitem
            if ($questionItemId != null) {
                return $this->redirectToRoute('anchor_questionitem_new', ['questionItemId' => $questionItemId]);
            }
            return $this->redirectToRoute('anchor_index');
        }

        return $this->render('anchor/new.html.twig', [
            'anchor' => $anchor,
            'relationTo' => 'questionItem',
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/new/algorithm/{algorithmId}", name="anchor_algorithm_new", requirements={"algorithmId"="\d+"}, methods={"GET","POST"})
     */
    public function newWithRelToAlgorithm(Request $request, GuidelineService $guidelineService, AlgorithmRepository $algorithmRepository, GuidelineRepository $guidelineRepository, $algorithmId = null): Response
    {
        $anchor = new Anchor();
        if ($algorithmId != null) {

            // in case user came here with a guideline in mind preset the form
            $algorithm = $algorithmRepository->findOneBy(['id' => intval($algorithmId)]);
            $anchor->setAlgorithm($algorithm);
            $form = $this->createForm(AnchorType::class, $anchor, ['empty_data' => ['guidelines' => $algorithm->getQuestionaire()->getGuidelines()]]);
            $form->handleRequest($request);
        } else {
            $form = $this->createForm(AnchorType::class, $anchor);
            $form->handleRequest($request);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($anchor);
            $entityManager->flush();
            $guidelineService->addAnchorToGuideline($anchor);

            // let user create multiple anchors in one session if he is comming from a questionitem
            if ($algorithmId != null) {
                return $this->redirectToRoute('anchor_algorithm_new', ['algorithmId' => $algorithmId]);
            }
            return $this->redirectToRoute('anchor_index');
        }

        return $this->render('anchor/new.html.twig', [
            'anchor' => $anchor,
            'relationTo'=> 'algorithm',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="anchor_show", methods={"GET"})
     */
    public function show(Anchor $anchor): Response
    {
        return $this->render('anchor/show.html.twig', [
            'anchor' => $anchor,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="anchor_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Anchor $anchor, GuidelineService $guidelineService, AnchorService $anchorService): Response
    {
        $form = $this->createForm(AnchorType::class, $anchor, ['empty_data' => ['guidelines' => [$anchor->getGuideline()]]]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($anchorService->removeSameAnchorFromAnchorText($anchor));
            $this->getDoctrine()->getManager()->flush();

            // remove old anchor and add new one to guideline
            $guidelineService->removeAnchorFromGuideline($anchor);
            $guidelineService->addAnchorToGuideline($anchor);
            return $this->redirectToRoute('anchor_index');
        }

        return $this->render('anchor/edit.html.twig', [
            'anchor' => $anchor,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="anchor_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Anchor $anchor, GuidelineService $guidelineService): Response
    {
        if ($this->isCsrfTokenValid('delete' . $anchor->getId(), $request->request->get('_token'))) {
            $guidelineService->removeAnchorFromGuideline($anchor);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($anchor);
            $entityManager->flush();
        }

        return $this->redirectToRoute('anchor_index');
    }
}