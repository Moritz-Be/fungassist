<?php

namespace App\Controller;

use App\Entity\GuidelineFigure;
use App\Form\GuidelineFigureType;
use App\Repository\GuidelineFigureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/guidelinefigure")
 */
class GuidelineFigureController extends AbstractController
{
    /**
     * @Route("/", name="guideline_figure_index", methods={"GET"})
     */
    public function index(GuidelineFigureRepository $guidelineFigureRepository): Response
    {
        return $this->render('guideline_figure/index.html.twig', [
            'guideline_figures' => $guidelineFigureRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="guideline_figure_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $guidelineFigure = new GuidelineFigure();
        $form = $this->createForm(GuidelineFigureType::class, $guidelineFigure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($guidelineFigure);
            $entityManager->flush();

            return $this->redirectToRoute('guideline_figure_index');
        }

        return $this->render('guideline_figure/new.html.twig', [
            'guideline_figure' => $guidelineFigure,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guideline_figure_show", methods={"GET"})
     */
    public function show(GuidelineFigure $guidelineFigure): Response
    {
        return $this->render('guideline_figure/show.html.twig', [
            'guideline_figure' => $guidelineFigure,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="guideline_figure_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, GuidelineFigure $guidelineFigure): Response
    {
        $form = $this->createForm(GuidelineFigureType::class, $guidelineFigure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('guideline_figure_index');
        }

        return $this->render('guideline_figure/edit.html.twig', [
            'guideline_figure' => $guidelineFigure,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="guideline_figure_delete", methods={"DELETE"})
     */
    public function delete(Request $request, GuidelineFigure $guidelineFigure): Response
    {
        if ($this->isCsrfTokenValid('delete' . $guidelineFigure->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($guidelineFigure);
            $entityManager->flush();
        }

        return $this->redirectToRoute('guideline_figure_index');
    }
}
