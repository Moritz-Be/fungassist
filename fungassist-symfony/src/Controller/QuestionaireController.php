<?php

namespace App\Controller;

use App\Entity\Questionaire;
use App\Entity\QuestionItem;
use App\Form\QuestionaireType;
use App\Repository\QuestionaireRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/backend/questionaire")
 */
class QuestionaireController extends AbstractController
{
    /**
     * @Route("/", name="questionaire_index", methods={"GET"})
     */
    public function index(QuestionaireRepository $questionaireRepository): Response
    {
        return $this->render('questionaire/index.html.twig', [
            'questionaires' => $questionaireRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="questionaire_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $questionaire = new Questionaire();
        $form = $this->createForm(QuestionaireType::class, $questionaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // for each new Questionaire create a new parent question Item
            $parentQuestionItem = new QuestionItem();
            $parentQuestionItem->setQuestionaire($questionaire);
            $parentQuestionItem->setParentId(0);
            $parentQuestionItem->setContent($questionaire->getTitle());
            $parentQuestionItem->setType(QuestionItem::TYPE["PARENT"]);
            $parentQuestionItem->setItemAbove(0);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parentQuestionItem);
            $entityManager->persist($questionaire);
            $entityManager->flush();

            return $this->redirectToRoute('questionaire_index');
        }

        return $this->render('questionaire/new.html.twig', [
            'questionaire' => $questionaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="questionaire_show", methods={"GET"})
     */
    public function show(Questionaire $questionaire): Response
    {
        return $this->render('questionaire/show.html.twig', [
            'questionaire' => $questionaire,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="questionaire_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Questionaire $questionaire): Response
    {
        $form = $this->createForm(QuestionaireType::class, $questionaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('questionaire_index');
        }

        return $this->render('questionaire/edit.html.twig', [
            'questionaire' => $questionaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="questionaire_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Questionaire $questionaire): Response
    {
        if ($this->isCsrfTokenValid('delete' . $questionaire->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($questionaire);
            $entityManager->flush();
        }

        return $this->redirectToRoute('questionaire_index');
    }
}
