<?php


namespace App\Controller;

use App\Repository\QuestionaireRepository;
use App\Repository\QuestionaireResultRepository;
use App\Repository\QuestionItemRepository;
use App\Service\QuestionItemService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route ("/cases")
 */
class CaseController extends AbstractController
{
    /**
     * * @Route ("/", name="case_index")
     */
    public function index(QuestionaireRepository $questionaireRepository){

        $this->denyAccessUnlessGranted('ROLE_CONTRIBUTOR');

        return $this->render('case/index.html.twig', [
            'questionaires' => $questionaireRepository->findAll()
        ]);
    }

    /**
     * * @Route ("/{questionaire}", name="case_index_questionaire")
     */
    public function indexQuestionaire($questionaire, QuestionaireRepository $questionaireRepository, QuestionaireResultRepository $questionaireResultRepository, QuestionItemRepository $questionItemRepository, QuestionItemService $questionItemService){

        $this->denyAccessUnlessGranted('ROLE_CONTRIBUTOR');

        $questionaire= $questionaireRepository->findOneBy(['title'=> $questionaire]);

        $questionaireResults = $questionaireResultRepository->findBy(['questionaire' => $questionaire]);

        $caseReports=[];
        foreach($questionaireResults as $questionaireResult){
            $caseReports[$questionaireResult->getId()]= $questionItemService->generatePatientReport($questionaireResult);
        }

        return $this->render('case/index_by_questionaire.html.twig', [
                'questionaireResults' => $questionaireResults,
                'parentQuestionItems' => $questionItemRepository->getPossibleQuestionItemParents($questionaire->getId()),
                'caseReports' => $caseReports
            ]
        );
    }

}