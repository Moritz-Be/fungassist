# fungassist

This is the repository for developping FungAssist




# Installation

## Preface

Consider using Phpstorm as your IDE and install the `Symfony Plugin`, `PHP Annotations` and `PHP Toolbox`.
Don't forget to enable the symfony Plugin for this project!

And: in the settings of Phpstorm go to *Composer* and select the composer.json of the project.



First, clone this repository:

```bash
$ git clone https://gitlab.com/infektiologie-ukkoeln/fungassist.git
```

Add `fungassist.local` in your `/etc/hosts` file.

Install  `docker` and  `docker-compose`

Install vendor packages from `fungassist-symfony` directory and make the cache / log folder available.

```bash
$ composer install && chmod -R 777 var/*

```

You might need to bash into the docker php container and set the priviliges there as well to 777 for the logs.

Then from the project's home directory, run:

```bash
$ docker-compose up
```

You are done, you can visit your Symfony application on the following URL: `http://fungassist.local` (and access Kibana on `http://fungassist.local:81`)

_Note :_ you can rebuild all Docker images by running:

```bash
$ docker-compose build
```
# Developer-Setup
We want our git-commit-msg to be useful. Thus there is a git commit hook that automatically adds the branch-name before the message.

Move the file fungassist/developer-setup/commit-msg to fungassist/.git/hooks and it should work :)

# How it works?

Here are the `docker-compose` built images:

* `db`: This is the MySQL database container (can be changed to postgresql or whatever in `docker-compose.yml` file),
* `php`: This is the PHP-FPM container including the application volume mounted on,
* `nginx`: This is the Nginx webserver container in which php volumes are mounted too,
* `elk`: This is a ELK stack container which uses Logstash to collect logs, send them into Elasticsearch and visualize them with Kibana.

This results in the following running containers:

```bash
> $ docker-compose ps
        Name                       Command               State              Ports
--------------------------------------------------------------------------------------------
dockersymfony_db_1      docker-entrypoint.sh mysqld      Up      0.0.0.0:3306->3306/tcp
dockersymfony_elk_1     /usr/bin/supervisord -n -c ...   Up      0.0.0.0:81->80/tcp
dockersymfony_nginx_1   nginx                            Up      443/tcp, 0.0.0.0:80->80/tcp
dockersymfony_php_1     php-fpm7 -F                      Up      0.0.0.0:9000->9000/tcp
```

# Read logs

You can access Nginx and Symfony application logs in the following directories on your host machine:

* `logs/nginx`
* `logs/symfony`

# Use Kibana!

You can also use Kibana to visualize Nginx & Symfony logs by visiting `http://symfony.localhost:81`.

# Use xdebug!

To use xdebug change the line `"docker-host.localhost:127.0.0.1"` in docker-compose.yml and replace 127.0.0.1 with your machine ip addres.
If your IDE default port is not set to 5902 you should do that, too.


# Server deploy
Do your server security setup (e.g. having a dedicated user, ssh access, ...)

Install composer `apt install composer php-xml`.

Clone the directory
`git clone https://gitlab.com/infektiologie-ukkoeln/fungassist.git`

Adjust the servername from 'fungassist.local' to whatever ip / domain-name in `nginx/fungassist-symfony.conf`. 

Adjust the database password in docker-compose.yml: `MYSQL_PASSWORD:XXX`

Make a copy of the .env file in fungassist symfony

- `cp .env .env.local`
- `APP_ENV=prod` // set environment to prod(you might want to start of with dev to get helpful debugging advice)
- `DATABASE_URL=mysql://fungassist:YOURPASSWORD@db:3306/fungassist` // Adjust the database password
- `SENDGRID_KEY=XXX` // Add the sendgrid API Key
- `SENTRY_DSN=` // add the sentry DSN key for the respective environment

Run `composer dump-env prod` to dump all env variables to one file, double check and save some server power.

Run `bin/phpunit` to execute all the tests.

Execute a composer install in the fungassist-symfony directory (the composer.lock file must be correct!).

You might need to install CKEditor Bundle: `php bin/console ckeditor:install && bin/console assets:install`

Pull up the containers by executing `docker-compose up` from the project directory.

You have to bash into your php-docker-container once with docker exec -it container-id bash and modify the var/ directory rights:
`docker ps (get php container id)`
`docker exec -it container-id bash`
`chmod -R 777 var/*`

Run a `php bin/console cache:warmup`, from the docker container.



# Code license

To be announced

# Thanks to 
Docker Setup by https://github.com/eko/docker-symfony
